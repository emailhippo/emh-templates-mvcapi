﻿// <copyright file="AppConfiguration.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Configuration
{
    /// <summary>
    /// The application configuration.
    /// </summary>
    /// <remarks>
    /// Note: This is the top level configuration node of the app domain configuration object graph.
    /// </remarks>
    public sealed class AppConfiguration
    {
        /// <summary>
        /// Gets or sets the setting1.
        /// </summary>
        /// <value>
        /// The setting1.
        /// </value>
        public string Setting1 { get; set; }

        /// <summary>
        /// Gets or sets the setting2.
        /// </summary>
        /// <value>
        /// The setting2.
        /// </value>
        public string Setting2 { get; set; }
    }
}