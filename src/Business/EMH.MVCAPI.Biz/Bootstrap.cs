﻿// <copyright file="Bootstrap.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz
{
    /// <summary>
    /// The main app domain bootstrap.
    /// </summary>
    public static class Bootstrap
    {
        /// <summary>
        /// Initializes the app domain.
        /// </summary>
        public static void Initialize()
        {
            /*Place bootstrap initialization here*/

            /*e.g. global use, expensive objects such as Redis connections, HttpClients etc*/
        }
    }
}