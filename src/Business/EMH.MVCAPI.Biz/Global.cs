﻿// <copyright file="Global.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz
{
    /// <summary>
    /// Global, app domain objects for efficiency re-use.
    /// </summary>
    internal static class Global
    {
    }
}