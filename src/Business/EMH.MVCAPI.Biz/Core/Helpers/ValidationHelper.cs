﻿// <copyright file="ValidationHelper.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Core.Helpers
{
    using System;
    using System.Net.Mail;
    using JetBrains.Annotations;

    /// <summary>
    /// Validation Helper.
    /// </summary>
    public static class ValidationHelper
    {
        /// <summary>
        /// Determines whether [is email valid] [the specified email address].
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns>
        ///   <c>true</c> if [is email valid] [the specified email address]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEmailValid([CanBeNull] this string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return false;
            }

            if (emailAddress.Length > 254)
            {
                /*MS MailAddress is incorrect for this situation.*/
                return false;
            }

            try
            {
                var mailAddress = new MailAddress(emailAddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}