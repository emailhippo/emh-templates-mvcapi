﻿// <copyright file="EmailAddressHelpers.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Core.Helpers
{
    using JetBrains.Annotations;

    /// <summary>
    /// Email address helpers.
    /// </summary>
    public static class EmailAddressHelpers
    {
        /// <summary>
        /// The splitter.
        /// </summary>
        [NotNull]
        private static readonly char[] Splitter = { '@' };

        /// <summary>
        /// Parses and email address to user and host components.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns>
        /// <list type="table">
        /// <listheader>
        /// <term>#</term>
        /// <description>description</description>
        /// </listheader>
        /// <item>
        /// <term>Item1</term>
        /// <description>User (i.e. the bit before the '@')</description>
        /// </item>
        /// <item>
        /// <term>Item2</term>
        /// <description>Domain (i.e. the bit after the '@')</description>
        /// </item>
        /// </list>
        /// </returns>
        public static (string, string)? ParseToUserAndHost([CanBeNull] this string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return null;
            }

            if (!emailAddress.Contains("@"))
            {
                return (null, emailAddress);
            }

            var strings = emailAddress.Split(Splitter);

            if (strings.Length >= 2)
            {
                return (strings[0], strings[strings.Length - 1]);
            }

            return (null, emailAddress);
        }
    }
}