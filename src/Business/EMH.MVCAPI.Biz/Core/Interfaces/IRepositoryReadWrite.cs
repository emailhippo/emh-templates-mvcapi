﻿// <copyright file="IRepositoryReadWrite.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Core.Interfaces
{
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// The read / write repository interface.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    /// <remarks>
    /// <para>Use this in the business tier for interacting with read-only external data repositories.</para>
    /// <para>Testing: Integration tests (y). Unit tests (n).</para>
    /// </remarks>
    public interface IRepositoryReadWrite<TEntity, TKey>
        : IRepositoryReadOnly<TEntity, TKey>
    {
        /// <summary>
        /// Adds an entity to the repository asynchronously.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation that encapsulates the key of the inserted item.</returns>
        [ItemCanBeNull]
        [NotNull]
        Task<TKey> AddAsync([NotNull] TEntity entity, CancellationToken cancellationToken);

        /// <summary>
        /// Deletes the item from the repository asynchronously.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [NotNull]
        Task DeleteAsync([NotNull] TKey key, CancellationToken cancellationToken);

        /// <summary>
        /// Updates the entity asynchronously.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [NotNull]
        Task UpdateAsync([NotNull] TKey key, [NotNull] TEntity entity, CancellationToken cancellationToken);
    }
}