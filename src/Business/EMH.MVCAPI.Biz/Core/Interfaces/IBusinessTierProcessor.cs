﻿// <copyright file="IBusinessTierProcessor.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Core.Interfaces
{
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// Business tier processor interface.
    /// </summary>
    /// <typeparam name="TRequest">The type of the request.</typeparam>
    /// <typeparam name="TResponse">The type of the response.</typeparam>
    /// <remarks>
    /// <para>This interface defines the business tier operations.</para>
    /// <para>It is where the various discreet processes are defined.</para>
    /// <para>Inject this interface into the service tier implementation.</para>
    /// <para>Testing: Integration tests (n). Unit tests (y - Mock out calls to external systems or dependencies).</para>
    /// </remarks>
    public interface IBusinessTierProcessor<in TRequest, TResponse>
    {
        /// <summary>
        /// Processes business tier request the asynchronously.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [ItemCanBeNull]
        [NotNull]
        Task<TResponse> ProcessAsync([NotNull] TRequest request, CancellationToken cancellationToken);
    }
}