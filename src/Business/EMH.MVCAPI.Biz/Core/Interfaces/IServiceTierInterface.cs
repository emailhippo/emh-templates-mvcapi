﻿// <copyright file="IServiceTierInterface.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Core.Interfaces
{
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// The service interface.
    /// </summary>
    /// <typeparam name="TRequest">The type of the request.</typeparam>
    /// <typeparam name="TResponse">The type of the response.</typeparam>
    /// <remarks>
    /// <para>This interface defines the top level service.</para>
    /// <para>It is an aggregation point for business logic interfaces.</para>
    /// <para>Inject this interface into the host Controller.</para>
    /// <para>Testing: Integration tests (y). Unit tests (y for flow operations using mocks for aggregated business tier objects).</para>
    /// </remarks>
    public interface IServiceTierInterface<in TRequest, TResponse>
    {
        /// <summary>
        /// Processes the service request asynchronously.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [ItemCanBeNull]
        [NotNull]
        Task<TResponse> ProcessServiceRequestAsync([NotNull] TRequest request, CancellationToken cancellationToken);
    }
}