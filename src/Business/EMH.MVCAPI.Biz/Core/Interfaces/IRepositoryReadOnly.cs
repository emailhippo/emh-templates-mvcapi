﻿// <copyright file="IRepositoryReadOnly.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Core.Interfaces
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// The read only repository interface.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    /// <remarks>
    /// <para>Use this in the business tier for interacting with read-only external data repositories.</para>
    /// <para>Testing: Integration tests (y). Unit tests (n).</para>
    /// </remarks>
    public interface IRepositoryReadOnly<TEntity, in TKey>
    {
        /// <summary>
        /// Gets all items asynchronously.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [CanBeNull]
        [ItemNotNull]
        Task<IEnumerable<TEntity>> GetAllAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Gets all items with paging asynchronously.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="itemsPerPage">The items per page.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [CanBeNull]
        [ItemNotNull]
        Task<IEnumerable<TEntity>> GetAllAsync(int pageNumber, int itemsPerPage, CancellationToken cancellationToken);

        /// <summary>
        /// Gets an item by key asynchronously.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [NotNull]
        [ItemCanBeNull]
        Task<TEntity> GetAsync([NotNull] TKey key, CancellationToken cancellationToken);
    }
}