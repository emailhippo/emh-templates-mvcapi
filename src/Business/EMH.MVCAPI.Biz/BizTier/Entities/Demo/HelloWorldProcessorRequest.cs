﻿// <copyright file="HelloWorldProcessorRequest.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.BizTier.Entities.Demo
{
    /// <summary>
    /// Hello World Processor Request.
    /// </summary>
    public sealed class HelloWorldProcessorRequest
    {
        /// <summary>
        /// Gets or sets the identifier to get.
        /// </summary>
        /// <value>
        /// The identifier to get.
        /// </value>
        public int IdToGet { get; set; }
    }
}