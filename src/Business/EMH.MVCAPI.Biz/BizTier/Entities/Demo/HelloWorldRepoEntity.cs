﻿// <copyright file="HelloWorldRepoEntity.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.BizTier.Entities.Demo
{
    using JetBrains.Annotations;

    /// <summary>
    /// Demo entity.
    /// <remarks>
    /// This class is a POCO that could represent an entity coming from external storage systems such as a database.
    /// </remarks>
    /// </summary>
    public sealed class HelloWorldRepoEntity
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [CanBeNull]
        public string Value { get; set; }
    }
}