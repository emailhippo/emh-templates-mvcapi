﻿// <copyright file="HelloWorldProcessorResponse.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.BizTier.Entities.Demo
{
    using JetBrains.Annotations;

    /// <summary>
    /// Hello World Processor Response.
    /// </summary>
    public sealed class HelloWorldProcessorResponse
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        [CanBeNull]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the total items in list.
        /// </summary>
        /// <value>
        /// The total items in list.
        /// </value>
        public int TotalItemsInList { get; set; }
    }
}