﻿// <copyright file="HelloWorldReadOnlyRepository.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.BizTier.Implementations.Demo
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using EMH.Lib.Patterns.ExceptionProcessor.Interface;
    using EMH.Lib.Patterns.HippoLog.Core;
    using EMH.MVCAPI.Biz.BizTier.Entities.Demo;
    using EMH.MVCAPI.Biz.Core.Interfaces;
    using EMH.MVCAPI.Biz.RetryStrategies;
    using JetBrains.Annotations;
    using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;

    /// <summary>
    /// Hello World Read Only Repository.
    /// </summary>
    /// <remarks>
    /// This demonstration repository uses hard coded returns to illustrate the data. However, in a real situation, this is where we would access storage data from, for example, a database.
    /// </remarks>
    public sealed class HelloWorldReadOnlyRepository : IRepositoryReadOnly<HelloWorldRepoEntity, int>
    {
        /// <summary>
        /// My retry strategy.
        /// </summary>
        [NotNull]
        private static readonly RetryStrategy MyRetryStrategy = new ExponentialBackoff();

        /// <summary>
        /// The exception processor.
        /// </summary>
        [NotNull]
        private readonly IExceptionProcessor exceptionProcessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelloWorldReadOnlyRepository"/> class.
        /// </summary>
        /// <param name="exceptionProcessor">The exception processor.</param>
        public HelloWorldReadOnlyRepository(
            [NotNull] IExceptionProcessor exceptionProcessor)
        {
            this.exceptionProcessor = exceptionProcessor;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<HelloWorldRepoEntity>> GetAllAsync(CancellationToken cancellationToken)
        {
            HippoLog.LogVerbose("Method enter:{0}:{1}", this.GetType(), "GetAllAsync");

            /*Note: This example uses a generic retry strategy. For SQL server resources, there is a default policy available by installing Nuget package Rsft.EntLib.TransientFaultHandling.Data*/
            var retryPolicy = new RetryPolicy<ExampleCatchAllRetryStrategy>(MyRetryStrategy);

            /*Faked external system dependency. In a real scenario, this object graph would come from external data store (e.g database)*/
            var helloWorldRepoEntities = new List<HelloWorldRepoEntity>
            {
                new HelloWorldRepoEntity { Id = 1, Value = "Hello World 1" },
                new HelloWorldRepoEntity { Id = 2, Value = "Hello World 2" },
            };

            /*This is where external database calls etc would go*/
            /*The invocation does the following:
             - Wraps calls in a retry policy
             - Where execution falls beyond the retry policy and exceptions are still happening, apply the exception processor policy of 'Business Tier' (log and throw)
            */
            var rtn = await retryPolicy.ExecuteAsync(
                () => this.exceptionProcessor.ProcessAsync(
                () => Task.FromResult(helloWorldRepoEntities), ExceptionProcessorPolicies.BusinessTierPolicy), cancellationToken).ConfigureAwait(false);

            HippoLog.LogVerbose("Method exit:{0}:{1}. Result:{@2}", this.GetType(), "GetAllAsync", rtn);

            return rtn.AsEnumerable();
        }

        /// <inheritdoc />
        public Task<IEnumerable<HelloWorldRepoEntity>> GetAllAsync(int pageNumber, int itemsPerPage, CancellationToken cancellationToken)
        {
            /*This method is designed to pass paging parameters pageNumber and itemsPerPage to back end data store for efficient paging and scaling of data in application.*/
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        public async Task<HelloWorldRepoEntity> GetAsync(int key, CancellationToken cancellationToken)
        {
            HippoLog.LogVerbose("Method enter:{0}:{1}. Key:{2}", this.GetType(), "GetAsync", key);

            var helloWorldRepoEntities = await this.GetAllAsync(cancellationToken).ConfigureAwait(false);

            var helloWorldRepoEntity = this.exceptionProcessor.Process(() => helloWorldRepoEntities.SingleOrDefault(r => r.Id == key), ExceptionProcessorPolicies.BusinessTierPolicy);

            HippoLog.LogVerbose("Method exit:{0}:{1}. Result:{@2}", this.GetType(), "GetAsync", helloWorldRepoEntity);

            return helloWorldRepoEntity;
        }
    }
}