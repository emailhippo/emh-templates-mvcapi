﻿// <copyright file="HelloWorldProcessor.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.BizTier.Implementations.Demo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using EMH.Lib.Patterns.ExceptionProcessor.Interface;
    using EMH.Lib.Patterns.HippoLog.Core;
    using EMH.MVCAPI.Biz.BizTier.Entities.Demo;
    using EMH.MVCAPI.Biz.Core.Interfaces;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Caching.Memory;

    /// <summary>
    /// Hello World Processor.
    /// </summary>
    /// <remarks>
    /// This example shows data interaction together with a caching implementation.
    /// </remarks>
    public sealed class HelloWorldProcessor : IBusinessTierProcessor<HelloWorldProcessorRequest, HelloWorldProcessorResponse>
    {
        /// <summary>
        /// The cache key.
        /// </summary>
        private const string CacheKey = @"B8D5D54A-ACF9-483A-945F-905671257362";

        /// <summary>
        /// My memory cache.
        /// </summary>
        [NotNull]
        private static readonly MemoryCache MyMemoryCache = new MemoryCache(new MemoryCacheOptions());

        /// <summary>
        /// The exception processor.
        /// </summary>
        [NotNull]
        private readonly IExceptionProcessor exceptionProcessor;

        /// <summary>
        /// The repository.
        /// </summary>
        [NotNull]
        private readonly IRepositoryReadOnly<HelloWorldRepoEntity, int> repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelloWorldProcessor"/> class.
        /// </summary>
        /// <param name="exceptionProcessor">The exception processor.</param>
        /// <param name="repository">The repository.</param>
        public HelloWorldProcessor(
            [NotNull] IExceptionProcessor exceptionProcessor,
            [NotNull] IRepositoryReadOnly<HelloWorldRepoEntity, int> repository)
        {
            this.exceptionProcessor = exceptionProcessor;
            this.repository = repository;
        }

        /// <inheritdoc />
        public async Task<HelloWorldProcessorResponse> ProcessAsync(HelloWorldProcessorRequest request, CancellationToken cancellationToken)
        {
            HippoLog.LogVerbose("Method enter:{0}:{1}. Request:{@2}", this.GetType(), "ProcessAsync", request);

            var list = await MyMemoryCache.GetOrCreateAsync(CacheKey, async _ =>
            {
                var cacheValue = _.Value;
                if (cacheValue != null)
                {
                    var value = (List<(int, string)>)cacheValue;

                    return value;
                }

                _.AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(10); // TODO: Set expiration to something sensible for use case..
                var newItems = await this.GetCurrentDataAsync(cancellationToken).ConfigureAwait(false);
                var list1 = newItems?.ToList();
                _.Value = list1;

                if (list1 == null || !list1.Any())
                {
                    HippoLog.LogError("Items from repo are null. There is a problem! Consult exception logs for further details.");
                }
                else
                {
                    HippoLog.LogVerbose("Cache refresh event. {0} items loaded into cache.", list1.Count);
                }

                return list1;
            }).ConfigureAwait(false);

            var firstOrDefault = list.FirstOrDefault(r => r.Item1 == request.IdToGet);

            var helloWorldProcessorResponse = new HelloWorldProcessorResponse { Message = firstOrDefault.Item2 ?? string.Empty, TotalItemsInList = list.Count };

            HippoLog.LogVerbose("Method exit: {0}:{1}. Response:{2}", this.GetType(), "ProcessAsync", helloWorldProcessorResponse);

            return helloWorldProcessorResponse;
        }

        /// <summary>
        /// Gets the current data asynchronously.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>
        /// A <see cref="Task" /> representing the asynchronous operation.
        /// </returns>
        [NotNull]
        [ItemCanBeNull]
        private async Task<IEnumerable<(int, string)>> GetCurrentDataAsync(CancellationToken cancellationToken)
        {
            var helloWorldRepoEntities
                = await this.repository.GetAllAsync(cancellationToken).ConfigureAwait(false);

            return helloWorldRepoEntities.Select(helloWorldRepoEntity => (helloWorldRepoEntity.Id, helloWorldRepoEntity.Value)).ToList();
        }
    }
}