﻿// <copyright file="HelloWorldServiceResponse.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.ServiceTier.Entities.Demo
{
    using JetBrains.Annotations;

    /// <summary>
    /// Hello World Service Response.
    /// </summary>
    public sealed class HelloWorldServiceResponse
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        [CanBeNull]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the total items in list.
        /// </summary>
        /// <value>
        /// The total items in list.
        /// </value>
        public int TotalItemsInList { get; set; }
    }
}