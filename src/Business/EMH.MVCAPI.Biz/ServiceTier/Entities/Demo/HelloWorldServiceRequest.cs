﻿// <copyright file="HelloWorldServiceRequest.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.ServiceTier.Entities.Demo
{
    /// <summary>
    /// Hello World Service Request.
    /// </summary>
    public sealed class HelloWorldServiceRequest
    {
        /// <summary>
        /// Gets or sets the identifier to get.
        /// </summary>
        /// <value>
        /// The identifier to get.
        /// </value>
        public int IdToGet { get; set; }
    }
}