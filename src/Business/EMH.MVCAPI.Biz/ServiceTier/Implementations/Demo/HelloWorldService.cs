﻿// <copyright file="HelloWorldService.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.ServiceTier.Implementations.Demo
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using EMH.Lib.Patterns.ExceptionProcessor.Interface;
    using EMH.Lib.Patterns.HippoLog.Core;
    using EMH.MVCAPI.Biz.BizTier.Entities.Demo;
    using EMH.MVCAPI.Biz.Core.Interfaces;
    using EMH.MVCAPI.Biz.ServiceTier.Entities.Demo;
    using JetBrains.Annotations;
    using SimpleMapper.Core;

    /// <summary>
    /// The Hello World Service implementation.
    /// </summary>
    /// <remarks>
    /// The service tier implementation is an aggregation point for the business logic components together with entity maps to map across biz/service architectural boundaries.
    /// </remarks>
    public sealed class HelloWorldService : IServiceTierInterface<HelloWorldServiceRequest, HelloWorldServiceResponse>
    {
        /// <summary>
        /// The exception processor.
        /// </summary>
        [NotNull]
        private readonly IExceptionProcessor exceptionProcessor;

        /// <summary>
        /// The processor.
        /// </summary>
        [NotNull]
        private readonly IBusinessTierProcessor<HelloWorldProcessorRequest, HelloWorldProcessorResponse> processor;

        /// <summary>
        /// The SVC to biz request mapper.
        /// </summary>
        [NotNull]
        private readonly IMapper<HelloWorldServiceRequest, HelloWorldProcessorRequest> svcToBizRequestMapper;

        /// <summary>
        /// The biz to SVC response mapper.
        /// </summary>
        [NotNull]
        private readonly IMapper<HelloWorldProcessorResponse, HelloWorldServiceResponse> bizToSvcResponseMapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelloWorldService"/> class.
        /// </summary>
        /// <param name="exceptionProcessor">The exception processor.</param>
        /// <param name="processor">The processor.</param>
        /// <param name="svcToBizRequestMapper">The SVC to biz request mapper.</param>
        /// <param name="bizToSvcResponseMapper">The biz to SVC response mapper.</param>
        /// <exception cref="ArgumentNullException">
        /// loggerFactory
        /// or
        /// exceptionProcessor
        /// or
        /// processor
        /// or
        /// svcToBizRequestMapper
        /// or
        /// svcToBizResponseMapper
        /// or
        /// bizToSvcRequestMapper
        /// or
        /// bizToSvcResponseMapper.
        /// </exception>
        public HelloWorldService(
            [NotNull] IExceptionProcessor exceptionProcessor,
            [NotNull] IBusinessTierProcessor<HelloWorldProcessorRequest, HelloWorldProcessorResponse> processor,
            [NotNull] IMapper<HelloWorldServiceRequest, HelloWorldProcessorRequest> svcToBizRequestMapper,
            [NotNull] IMapper<HelloWorldProcessorResponse, HelloWorldServiceResponse> bizToSvcResponseMapper)
        {
            this.exceptionProcessor = exceptionProcessor ?? throw new ArgumentNullException(nameof(exceptionProcessor));
            this.processor = processor ?? throw new ArgumentNullException(nameof(processor));
            this.svcToBizRequestMapper = svcToBizRequestMapper ?? throw new ArgumentNullException(nameof(svcToBizRequestMapper));
            this.bizToSvcResponseMapper = bizToSvcResponseMapper ?? throw new ArgumentNullException(nameof(bizToSvcResponseMapper));
        }

        /// <inheritdoc />
        public async Task<HelloWorldServiceResponse> ProcessServiceRequestAsync(HelloWorldServiceRequest request, CancellationToken cancellationToken)
        {
            HippoLog.LogVerbose("Method enter:{0}:{1}. Request:{@2}", this.GetType(), "ProcessServiceRequestAsync", request);

            /*Map (in)*/
            var helloWorldProcessorRequest = this.svcToBizRequestMapper.Map(request);

            /*Execute*/
            var helloWorldProcessorResponse
                = await this.exceptionProcessor.ProcessAsync(
                    () => this.processor.ProcessAsync(helloWorldProcessorRequest, cancellationToken),
                    ExceptionProcessorPolicies.LogAndResumePolicy).ConfigureAwait(false);

            /*Map (out)*/
            var helloWorldServiceResponse = this.bizToSvcResponseMapper.Map(helloWorldProcessorResponse);

            HippoLog.LogVerbose("Method exit: {0}:{1}. Response:{2}", this.GetType(), "ProcessServiceRequestAsync", helloWorldProcessorResponse);

            return helloWorldServiceResponse;
        }
    }
}