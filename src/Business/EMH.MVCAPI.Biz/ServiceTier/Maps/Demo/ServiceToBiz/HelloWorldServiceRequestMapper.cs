﻿// <copyright file="HelloWorldServiceRequestMapper.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.ServiceTier.Maps.Demo.ServiceToBiz
{
    using EMH.MVCAPI.Biz.BizTier.Entities.Demo;
    using EMH.MVCAPI.Biz.ServiceTier.Entities.Demo;
    using SimpleMapper.Core;

    /// <summary>
    /// Hello World Service Request Mapper.
    /// </summary>
    public sealed class HelloWorldServiceRequestMapper : IMapper<HelloWorldServiceRequest, HelloWorldProcessorRequest>
    {
        /// <inheritdoc />
        public HelloWorldProcessorRequest Map(HelloWorldServiceRequest source)
        {
            return source == null ? null : new HelloWorldProcessorRequest { IdToGet = source.IdToGet };
        }
    }
}