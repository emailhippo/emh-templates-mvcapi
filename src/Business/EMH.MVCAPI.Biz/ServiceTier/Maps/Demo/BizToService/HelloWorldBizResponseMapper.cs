﻿// <copyright file="HelloWorldBizResponseMapper.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.ServiceTier.Maps.Demo.BizToService
{
    using SimpleMapper.Core;

    /// <summary>
    /// Hello World Service Request Mapper.
    /// </summary>
    public sealed class HelloWorldBizResponseMapper : IMapper<BizTier.Entities.Demo.HelloWorldProcessorResponse, Entities.Demo.HelloWorldServiceResponse>
    {
        /// <inheritdoc />
        public Entities.Demo.HelloWorldServiceResponse Map(BizTier.Entities.Demo.HelloWorldProcessorResponse source)
        {
            if (source == null)
            {
                return null;
            }

            return new Entities.Demo.HelloWorldServiceResponse { Message = source.Message, TotalItemsInList = source.TotalItemsInList };
        }
    }
}