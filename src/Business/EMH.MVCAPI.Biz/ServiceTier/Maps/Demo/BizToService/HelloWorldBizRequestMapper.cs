﻿// <copyright file="HelloWorldBizRequestMapper.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.ServiceTier.Maps.Demo.BizToService
{
    using SimpleMapper.Core;

    /// <summary>
    /// Hello World Service Request Mapper.
    /// </summary>
    public sealed class HelloWorldBizRequestMapper : IMapper<BizTier.Entities.Demo.HelloWorldProcessorRequest, Entities.Demo.HelloWorldServiceRequest>
    {
        /// <inheritdoc />
        public Entities.Demo.HelloWorldServiceRequest Map(BizTier.Entities.Demo.HelloWorldProcessorRequest source)
        {
            return source == null ? null : new Entities.Demo.HelloWorldServiceRequest { IdToGet = source.IdToGet };
        }
    }
}