﻿// <copyright file="ExampleCatchAllRetryStrategy.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.RetryStrategies
{
    using System;
    using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;

    /// <summary>
    /// Example Retry Strategy..
    /// </summary>
    /// <remarks>
    /// <para>This example designates all exceptions as transient.</para>
    /// <para>For real scenarios, filter exceptions by type to define whether errors can be corrected by retry. See <see cref="https://docs.microsoft.com/en-us/previous-versions/msp-n-p/hh675232(v%3dpandp.10)"/>.</para>
    /// <para>For SQL Server, use built in RetryStrategy from <see cref="https://github.com/Rolosoft/Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling.Data.Standard"/>.</para>
    /// </remarks>
    /// <seealso cref="ITransientErrorDetectionStrategy" />
    internal sealed class ExampleCatchAllRetryStrategy : ITransientErrorDetectionStrategy
    {
        /// <inheritdoc />
        public bool IsTransient(Exception ex)
        {
            return ex.GetType() == typeof(Exception);
        }
    }
}