﻿// <copyright file="HelloWorldResult.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Host.Web.Models
{
    using System.Runtime.Serialization;
    using JetBrains.Annotations;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// This model is returned by the API.
    /// </summary>
    [DataContract]
    [ProtoContract]
    public sealed class HelloWorldResult
    {
        /// <summary>
        /// The message from the API.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        [CanBeNull]
        [ProtoMember(1)]
        [DataMember(Order = 1)]
        [JsonProperty(Order = 1, PropertyName = "message")]
        public string Message { get; set; }

        /// <summary>
        /// Any error message from the API.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        [CanBeNull]
        [ProtoMember(2)]
        [DataMember(Order = 2)]
        [JsonProperty(Order = 2, PropertyName = "error")]
        public string Error { get; set; }
    }
}