﻿// <copyright file="ProtobufOutputFormatter.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Host.Web.MyCode.MediaTypeFormatters.Protobuf
{
    using System;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.Net.Http.Headers;
    using ProtoBuf.Meta;

    /// <summary>
    /// Protobuf Output Formatter.
    /// </summary>
    /// <seealso cref="TextOutputFormatter" />
    public sealed class ProtobufOutputFormatter : OutputFormatter
    {
        /// <summary>
        /// The model value.
        /// </summary>
        [ItemNotNull]
        [NotNull]
        private static readonly Lazy<RuntimeTypeModel> ModelValue = new Lazy<RuntimeTypeModel>(CreateTypeModel);

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtobufOutputFormatter"/> class.
        /// </summary>
        public ProtobufOutputFormatter()
        {
            this.ContentType = "application/x-protobuf";
            this.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/x-protobuf"));
        }

        /// <summary>
        /// Gets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        [NotNull]
        public static RuntimeTypeModel Model => ModelValue.Value;

        /// <summary>
        /// Gets the type of the content.
        /// </summary>
        /// <value>
        /// The type of the content.
        /// </value>
        [NotNull]
        public string ContentType { get; }

        /// <inheritdoc />
        [NotNull]
        public override Task WriteResponseBodyAsync([NotNull] OutputFormatterWriteContext context)
        {
            var response = context.HttpContext.Response;

            Model.Serialize(response.Body, context.Object);
            return Task.FromResult(response);
        }

        /// <summary>
        /// Creates the type model.
        /// </summary>
        /// <returns>The <see cref="RuntimeTypeModel"/>.</returns>
        [NotNull]
        private static RuntimeTypeModel CreateTypeModel()
        {
            var typeModel = RuntimeTypeModel.Create();
            typeModel.UseImplicitZeroDefaults = false;
            return typeModel;
        }
    }
}