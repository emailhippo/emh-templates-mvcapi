﻿// <copyright file="BsonSerializerObjectPolicy.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Host.Web.MyCode.MediaTypeFormatters.Bson
{
    using JetBrains.Annotations;
    using Microsoft.Extensions.ObjectPool;
    using Newtonsoft.Json;

    /// <summary>
    /// BSON Serializer Object Policy.
    /// </summary>
    public class BsonSerializerObjectPolicy : IPooledObjectPolicy<JsonSerializer>
    {
        /// <summary>
        /// The serializer settings.
        /// </summary>
        [NotNull]
        private readonly JsonSerializerSettings serializerSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="BsonSerializerObjectPolicy"/> class.
        /// </summary>
        /// <param name="serializerSettings">The serializer settings.</param>
        public BsonSerializerObjectPolicy([NotNull] JsonSerializerSettings serializerSettings)
        {
            this.serializerSettings = serializerSettings;
        }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>The <see cref="JsonSerializer"/>.</returns>
        [NotNull]
        public JsonSerializer Create() => JsonSerializer.Create(this.serializerSettings);

        /// <summary>
        /// Returns the specified serializer.
        /// </summary>
        /// <param name="serializer">The serializer.</param>
        /// <returns>True if serializer.</returns>
        public bool Return([NotNull] JsonSerializer serializer) => true;
    }
}