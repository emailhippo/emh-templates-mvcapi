﻿// <copyright file="Program.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Host.Web
{
    using JetBrains.Annotations;
    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Serilog;

    /// <summary>
    /// The program entry point class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main([ItemNotNull][CanBeNull] string[] args)
        {
            var webHostBuilder = CreateWebHostBuilder(args);
            webHostBuilder.Run();
        }

        /// <summary>
        /// Creates the web host builder.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns>The IWebHost.</returns>
        private static IWebHost CreateWebHostBuilder([ItemNotNull][CanBeNull] string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel(
                    options =>
                    {
                        options.AddServerHeader = false;
                        options.AllowSynchronousIO = true;
                    })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    var hostingContextHostingEnvironment = hostingContext.HostingEnvironment;

                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));

                    if (!hostingContextHostingEnvironment.IsDevelopment())
                    {
                        return;
                    }

                    logging.AddConsole();
                    logging.AddDebug();
                })
                .UseStartup<Startup>()
                .UseSerilog()
                .Build();
    }
}
