﻿// <copyright file="HomeController.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Host.Web.Controllers
{
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The home controller.
    /// </summary>
    /// <seealso cref="Controller" />
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : Controller
    {
        /// <summary>
        /// Home index page.
        /// </summary>
        /// <returns>The <see cref="IActionResult"/>.</returns>
        [HttpGet]
        [Route("~/")]
        [ResponseCache(VaryByHeader = "User-Agent", Duration = 3600)]
        [NotNull]
        public IActionResult Index()
        {
            return this.Ok("Email Hippo API.");
        }

        /// <summary>
        /// Errors this instance.
        /// </summary>
        /// <returns>Error page.</returns>
        [HttpGet]
        [Route("~/Error")]
        [NotNull]
        public IActionResult Error()
        {
            return this.StatusCode(500, "server error");
        }
    }
}