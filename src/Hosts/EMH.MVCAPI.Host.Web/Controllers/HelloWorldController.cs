﻿// <copyright file="HelloWorldController.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Host.Web.Controllers
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using EMH.MVCAPI.Biz.Core.Interfaces;
    using EMH.MVCAPI.Biz.ServiceTier.Entities.Demo;
    using EMH.MVCAPI.Host.Web.Models;
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Mvc;
    using Swashbuckle.AspNetCore.Annotations;

    /// <summary>
    /// Hello World controller.
    /// </summary>
    [SwaggerTag("Email Hippo API Hello World example.")]
    [Route("api/helloworld/v1")]
    [AllowAnonymous]
    [ApiExplorerSettings(GroupName = "v1")]
    [EnableCors("AllowAllOrigins")]
    public sealed class HelloWorldController : Controller
    {
        /// <summary>
        /// The service tier.
        /// </summary>
        [NotNull]
        private readonly IServiceTierInterface<HelloWorldServiceRequest, HelloWorldServiceResponse> serviceTier;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelloWorldController"/> class.
        /// </summary>
        /// <param name="serviceTier">The service tier.</param>
        /// <exception cref="ArgumentNullException">serviceTier.</exception>
        public HelloWorldController([NotNull] IServiceTierInterface<HelloWorldServiceRequest, HelloWorldServiceResponse> serviceTier)
        {
            this.serviceTier = serviceTier ?? throw new ArgumentNullException(nameof(serviceTier));
        }

        /// <summary>
        /// A hello world example. For demonstration purposes only.
        /// </summary>
        /// <param name="query">Any query will do for demo purposes.</param>
        /// <returns>
        /// Return HelloWorldResult.
        /// </returns>
        /// <response code="400">Bad request. Request is empty/null or exceeds the maximum allowed length of 255 characters.</response>
        /// <response code="429">Maximum processing rate exceeded. Please slow your requests to &lt; 10 queries per second.</response>
        /// <response code="500">Server error.</response>
        [SwaggerOperation("Get a hello world message back from the API.")]
        [Produces("application/json", "application/xml", "application/bson", "application/x-protobuf")]
        [HttpGet]
        [Route("getmessage/{query}")]
        [ProducesResponseType(typeof(HelloWorldResult), 200)]
        public async Task<IActionResult> GetMessage(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                return this.BadRequest("No query supplied");
            }

            if (query.Length > 255)
            {
                return this.BadRequest("Query too long");
            }

            /*Hard coded request. Just to illustrate workings*/
            HelloWorldServiceResponse helloWorldServiceResponse = await this.serviceTier.ProcessServiceRequestAsync(new HelloWorldServiceRequest { IdToGet = 1 }, CancellationToken.None)
                .ConfigureAwait(false);

            /*Map to HelloWorldResult*/
            /*Note: Error is empty here. In real scenario, error would be a user friendly error message (i.e. not the raw server stack trace). Stack traces and exception logs will be available via logging defined in the Serilog->IExceptionProcessor stack*/
            HelloWorldResult helloWorldResult = new HelloWorldResult { Error = string.Empty, Message = helloWorldServiceResponse?.Message };

            if (!string.IsNullOrEmpty(helloWorldServiceResponse.Message))
            {
                return this.Ok(helloWorldResult);
            }

            /*Put meaningful, sanitized (for users) messages here*/
            return this.BadRequest("There was an error.");
        }
    }
}