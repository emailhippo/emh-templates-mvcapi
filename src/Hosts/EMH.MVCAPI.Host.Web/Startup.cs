﻿// <copyright file="Startup.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Host.Web
{
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Threading;
    using EMH.Lib.Patterns.ExceptionProcessor.Interface;
    using EMH.Lib.Patterns.ExceptionProcessor.Logging;
    using EMH.Lib.Patterns.HippoLog.Core;
    using EMH.Lib.Patterns.HippoLog.SerilogAdapter;
    using EMH.MVCAPI.Biz;
    using EMH.MVCAPI.Biz.BizTier.Entities.Demo;
    using EMH.MVCAPI.Biz.BizTier.Implementations.Demo;
    using EMH.MVCAPI.Biz.Configuration;
    using EMH.MVCAPI.Biz.Core.Interfaces;
    using EMH.MVCAPI.Biz.ServiceTier.Entities.Demo;
    using EMH.MVCAPI.Biz.ServiceTier.Implementations.Demo;
    using EMH.MVCAPI.Biz.ServiceTier.Maps.Demo.BizToService;
    using EMH.MVCAPI.Biz.ServiceTier.Maps.Demo.ServiceToBiz;
    using EMH.MVCAPI.Host.Web.MyCode.MediaTypeFormatters.Bson;
    using EMH.MVCAPI.Host.Web.MyCode.MediaTypeFormatters.Protobuf;
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.PlatformAbstractions;
    using Microsoft.Net.Http.Headers;
    using Microsoft.OpenApi.Models;
    using Serilog;
    using SimpleInjector;
    using SimpleInjector.Lifestyles;
    using SimpleMapper.Core;

    /// <summary>
    /// The startup class.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// The container.
        /// </summary>
        [NotNull]
        private static readonly Container Container = new Container
        {
            Options = { ResolveUnregisteredConcreteTypes = true, DefaultScopedLifestyle = new AsyncScopedLifestyle() },
        };

        /// <summary>
        /// The application configuration.
        /// </summary>
        [NotNull]
        private readonly AppConfiguration applicationConfiguration = new AppConfiguration();

        /// <summary>
        /// The minimum worker threads.
        /// </summary>
        private readonly int minWorkerThreads;

        /// <summary>
        /// The minimum completion port threads.
        /// </summary>
        private readonly int minCompletionPortThreads;

        /// <summary>
        /// The maximum worker threads.
        /// </summary>
        private readonly int maxWorkerThreads;

        /// <summary>
        /// The maximum completion port threads.
        /// </summary>
        private readonly int maxCompletionPortThreads;

        /// <summary>
        /// The default connection limit.
        /// </summary>
        private readonly int defaultConnectionLimit;

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup([NotNull] IConfiguration configuration)
        {
            ServicePointManager.DefaultConnectionLimit = int.MaxValue;
            ThreadPool.SetMinThreads(100, 100);

            ThreadPool.GetMaxThreads(out this.maxWorkerThreads, out this.maxCompletionPortThreads);
            ThreadPool.GetMinThreads(out this.minWorkerThreads, out this.minCompletionPortThreads);
            this.defaultConnectionLimit = ServicePointManager.DefaultConnectionLimit;

            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();

            var serilogAdapter = SerilogAdapterFactory.Create();
            HippoLog.Initialize(serilogAdapter);

            ExceptionProcessor = LoggingExceptionProcessorFactory.Create();

            configuration.Bind("AppConfiguration", this.applicationConfiguration);

            Configuration = configuration;
        }

        /// <summary>
        /// Gets or sets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        private static IConfiguration Configuration { get; set; }

        /// <summary>
        /// Gets or sets the exception processor.
        /// </summary>
        private static IExceptionProcessor ExceptionProcessor
        {
            [CanBeNull]
            get;
            [CanBeNull]
            set;
        }

        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <remarks>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940.
        /// </remarks>
        public void ConfigureServices([NotNull] IServiceCollection services)
        {
            /*Add framework services.*/
            services.AddCors(options =>
            {
                options.AddPolicy(
                    "AllowAllOrigins",
                    builder =>
                    {
                        builder.AllowAnyOrigin();
                        builder.AllowAnyHeader();
                        builder.AllowAnyMethod();
                    });
            });

            /*MVC and API services*/
            services
                .AddMvc(options =>
                {
                    options.OutputFormatters.Add(new XmlSerializerOutputFormatter());
                    options.InputFormatters.Add(new ProtobufInputFormatter()); // TODO: Remove if not required
                    options.OutputFormatters.Add(new ProtobufOutputFormatter()); // TODO: Remove if not required
                    options.FormatterMappings.SetMediaTypeMappingForFormat("protobuf", MediaTypeHeaderValue.Parse("application/x-protobuf")); // TODO: Remove if not required
                })
                .AddBsonSerializerFormatters()
                .SetCompatibilityVersion(CompatibilityVersion.Latest);

            services.AddControllersWithViews();

            services.AddRazorPages();

            services.AddSimpleInjector(
                Container,
                options =>
                {
                    options.AddAspNetCore()
                        .AddControllerActivation()
                        .AddViewComponentActivation()
                        .AddPageModelActivation()
                        .AddTagHelperActivation();
                });

            ConfigureSwagger(services);
        }

        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        /// <remarks>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </remarks>
        public void Configure(
            [NotNull] IApplicationBuilder app,
            [NotNull] IWebHostEnvironment env,
            [NotNull] ILoggerFactory loggerFactory)
        {
            app.UseSimpleInjector(Container);

            loggerFactory.AddSerilog();

            HippoLog.LogInformation(
                "Starting with minWorkerThreads={minWorkerThreads}, minCompletionPortThreads={minCompletionPortThreads}, maxWorkerThreads:{maxWorkerThreads}, maxCompletionPortThreads:{maxCompletionPortThreads}, defaultConnectionLimit:{defaultConnectionLimit}, .",
                this.minWorkerThreads,
                this.minCompletionPortThreads,
                this.maxWorkerThreads,
                this.maxCompletionPortThreads,
                this.defaultConnectionLimit);

            /*Bootstrap the app domain*/
            Bootstrap.Initialize();

            var policyCollection = new HeaderPolicyCollection()
                .AddDefaultSecurityHeaders()
                .AddCustomHeader("ARR-Disable-Session-Affinity", "true");

            app.UseSecurityHeaders(policyCollection);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            this.InitializeContainer(app);

            if (env.IsDevelopment())
            {
                Container.Verify();
            }

            app.UseDefaultFiles();

            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSecondsDefault = 60 * 60 * 12;
                    const int durationInSecondsFarForward = 60 * 60 * 24 * 90;

                    if (ctx.Context.Request.Path.Value.Contains("favicon.ico"))
                    {
                        ctx.Context.Response.Headers[HeaderNames.CacheControl] = string.Format(
                            CultureInfo.InvariantCulture,
                            "public,max-age={0}",
                            durationInSecondsFarForward);
                    }
                    else
                    {
                        ctx.Context.Response.Headers[HeaderNames.CacheControl] = string.Format(
                            CultureInfo.InvariantCulture,
                            "public,max-age={0}",
                            durationInSecondsDefault);
                    }
                },
            });

            app.UseRouting();
            app.UseDefaultFiles();
            app.UseStatusCodePages();

            app.UseCors(
                a =>
                {
                    a.AllowAnyOrigin();
                    a.AllowAnyHeader();
                    a.AllowAnyMethod();
                });

            app.UseEndpoints(
                endpoints =>
                {
                    endpoints.MapRazorPages();
                    endpoints.MapControllers();
                });

            app.UseSwagger(c => c.SerializeAsV2 = true);

            // TODO: Configure API name
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                c.DefaultModelsExpandDepth(1);
                c.DocumentTitle = @"Email Hippo : {API Name}";
            });
        }

        /// <summary>
        /// Configures the swagger.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        private static void ConfigureSwagger([ItemNotNull][NotNull] IServiceCollection configuration)
        {
            var info = new OpenApiInfo
            {
                Version = "v1",
                Title = "Email Hippo Development Standards Demo and Template For MVC API Projects",
                Description = string.Empty, // TODO: Configure Swagger description
            };

            configuration.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", info);

                var filePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "EMH.MVCAPI.Host.Web.xml");
                c.IncludeXmlComments(filePath);
                c.IgnoreObsoleteActions();
                c.EnableAnnotations();
            });
        }

        /// <summary>
        /// Initializes the container.
        /// </summary>
        /// <param name="app">The application.</param>
        private void InitializeContainer(
            [NotNull] IApplicationBuilder app)
        {
            Container.RegisterSingleton<IHttpContextAccessor, HttpContextAccessor>();

            Container.RegisterSingleton(() => ExceptionProcessor);
            Container.RegisterSingleton(() => this.applicationConfiguration);

            // TODO: Register custom dependencies here

            /*Demo registrations, BEGIN*/
            Container.RegisterSingleton<IRepositoryReadOnly<HelloWorldRepoEntity, int>, HelloWorldReadOnlyRepository>();
            Container.RegisterSingleton<IBusinessTierProcessor<HelloWorldProcessorRequest, HelloWorldProcessorResponse>, HelloWorldProcessor>();
            Container.RegisterSingleton<IMapper<HelloWorldServiceRequest, HelloWorldProcessorRequest>, HelloWorldServiceRequestMapper>();
            Container.RegisterSingleton<IMapper<HelloWorldServiceResponse, HelloWorldProcessorResponse>, HelloWorldServiceResponseMapper>();
            Container.RegisterSingleton<IMapper<HelloWorldProcessorRequest, HelloWorldServiceRequest>, HelloWorldBizRequestMapper>();
            Container.RegisterSingleton<IMapper<HelloWorldProcessorResponse, HelloWorldServiceResponse>, HelloWorldBizResponseMapper>();
            Container.RegisterSingleton<IServiceTierInterface<HelloWorldServiceRequest, HelloWorldServiceResponse>, HelloWorldService>();
            /*Demo registrations, END*/
        }
    }
}
