﻿// <copyright file="ValidationHelperTests.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Tests.Core.Helpers
{
    using System.Diagnostics;
    using EMH.MVCAPI.Biz.Core.Helpers;
    using NUnit.Framework;

    /// <summary>
    /// Validation helper tests.
    /// </summary>
    /// <seealso cref="TestBase" />
    [TestFixture]
    public sealed class ValidationHelperTests : TestBase
    {
        /// <summary>
        /// Determines whether [is email valid using test items] [the specified email address].
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns>
        ///   <c>true</c> if [is email valid using test items] [the specified email address]; otherwise, <c>false</c>.
        /// </returns>
        [TestCase("test@test.com", ExpectedResult = true)]
        [TestCase("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@letters-in-local.org", ExpectedResult = true)]
        [TestCase("01234567890@numbers-in-local.net", ExpectedResult = true)]
        [TestCase("&'*+-./=?^_{}~@other-valid-characters-in-local.net", ExpectedResult = true)]
        [TestCase("mixed-1234-in-{+^}-local@sld.net", ExpectedResult = true)]
        [TestCase("a@single-character-in-local.org", ExpectedResult = true)]
        [TestCase("\"quoted\"@sld.com", ExpectedResult = true)]
        [TestCase("\"quoted-at-sign@sld.org\"@sld.com", ExpectedResult = true)]
        [TestCase("punycode-numbers-in-tld@sld.xn--3e0b707e", ExpectedResult = true)]
        [TestCase("test", ExpectedResult = false)]
        [TestCase("the-total-length@of-an-entire-address-cannot-be-longer-than-two-hundred-and-fifty-four-characters-and-this-address-is-254-characters-exactly-so-it-should-be-valid-and-im-going-to-add-some-more-words-here-to-increase-the-lenght-blah-blah-blah-blah-bla-bla.org", ExpectedResult = false)]
        [TestCase("@missing-local.org", ExpectedResult = false)]
        [TestCase("invalid-characters-in-sld@! \"#$%(),/;<>_[]`|.org", ExpectedResult = false)]
        /*[TestCase("missing-dot-before-tld@com", ExpectedResult = false)]
        [TestCase("missing-tld@sld.", ExpectedResult = false)]*/// NOTE: These 2 cases (valid as per RFCs) are incorrectly described by the Microsoft implementation.
        [Test]
        public bool IsEmailValid_UsingTestItems(string emailAddress)
        {
            var stopwatch = Stopwatch.StartNew();
            var isEmailValid = emailAddress.IsEmailValid();
            stopwatch.Stop();

            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);

            return isEmailValid;
        }
    }
}