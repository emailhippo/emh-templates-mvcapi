﻿// <copyright file="EmailAddressHelpersTests.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Tests.Core.Helpers
{
    using System.Collections.Generic;
    using EMH.MVCAPI.Biz.Core.Helpers;
    using JetBrains.Annotations;
    using NUnit.Framework;

    /// <summary>
    /// Email Address Helpers Tests.
    /// </summary>
    /// <seealso cref="TestBase" />
    [TestFixture]
    public sealed class EmailAddressHelpersTests : TestBase
    {
        /// <summary>
        /// Parses to user and host.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns>The tuple with the user and host information.</returns>
        [Test]
        [TestCaseSource(typeof(MyTestClass), nameof(MyTestClass.TestCases))]
        [CanBeNull]
        public (string, string)? ParseToUserAndHost_UsingTestCases_ExpectPass(string emailAddress)
        {
            return emailAddress.ParseToUserAndHost();
        }

        /// <summary>
        /// <see cref="https://github.com/nunit/docs/wiki/TestCaseData"/>.
        /// </summary>
        private class MyTestClass
        {
            public static IEnumerable<TestCaseData> TestCases
            {
                get
                {
                    yield return new TestCaseData("test@test.com").Returns(("test", "test.com"));
                    yield return new TestCaseData(null).Returns(null);
                    yield return new TestCaseData(string.Empty).Returns(null);
                    yield return new TestCaseData(string.Empty).Returns(null);
                    (string, string)? rtn1 = (null, "test");
                    yield return new TestCaseData("test").Returns(rtn1);
                }
            }
        }
    }
}