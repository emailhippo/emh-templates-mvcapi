﻿// <copyright file="HelloWorldReadOnlyRepositoryTests.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Tests.BizTier.Integration.Implementations.Demo
{
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using EMH.MVCAPI.Biz.BizTier.Implementations.Demo;
    using JetBrains.Annotations;
    using NUnit.Framework;

    /// <summary>
    /// Hello World Read Only Repository Tests.
    /// </summary>
    /// <seealso cref="TestBase" />
    /// <remarks>
    /// Demonstrates integration tests for the demo repo implementation.
    /// </remarks>
    [TestFixture]
    public sealed class HelloWorldReadOnlyRepositoryTests : TestBase
    {
        /// <summary>
        /// The system under test.
        /// </summary>
        [NotNull]
        private HelloWorldReadOnlyRepository sut;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelloWorldReadOnlyRepositoryTests"/> class.
        /// </summary>
        public HelloWorldReadOnlyRepositoryTests()
        {
            this.sut = new HelloWorldReadOnlyRepository(this.ExceptionProcessor);
        }

        /// <summary>
        /// Gets all asynchronous expect items.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        public async Task GetAllAsync_ExpectItems()
        {
            // Arrange

            // Act
            var stopwatch = Stopwatch.StartNew();
            var helloWorldRepoEntities = await this.sut.GetAllAsync(CancellationToken.None).ConfigureAwait(false);
            stopwatch.Stop();

            // Assert
            Assert.That(helloWorldRepoEntities.Count() == 2);
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// Gets the asynchronous when known item expect item.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        public async Task GetAsync_WhenKnownItem_ExpectItem()
        {
            // Arrange
            const int itemIdToFetch = 1;

            // Act
            var stopwatch = Stopwatch.StartNew();
            var helloWorldRepoEntity = await this.sut.GetAsync(itemIdToFetch, CancellationToken.None).ConfigureAwait(false);
            stopwatch.Stop();

            // Assert
            Assert.AreEqual(itemIdToFetch, helloWorldRepoEntity.Id);
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// Gets the asynchronous when known item expect null item.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        public async Task GetAsync_WhenUnKnownItem_ExpectNullItem()
        {
            // Arrange
            const int itemIdToFetch = 99;

            // Act
            var stopwatch = Stopwatch.StartNew();
            var helloWorldRepoEntity = await this.sut.GetAsync(itemIdToFetch, CancellationToken.None).ConfigureAwait(false);
            stopwatch.Stop();

            // Assert
            Assert.IsNull(helloWorldRepoEntity);
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }
    }
}