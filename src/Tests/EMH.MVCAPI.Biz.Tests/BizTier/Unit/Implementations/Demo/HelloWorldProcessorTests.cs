﻿// <copyright file="HelloWorldProcessorTests.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Tests.BizTier.Unit.Implementations.Demo
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using EMH.MVCAPI.Biz.BizTier.Entities.Demo;
    using EMH.MVCAPI.Biz.BizTier.Implementations.Demo;
    using EMH.MVCAPI.Biz.Core.Interfaces;
    using JetBrains.Annotations;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Hello World Processor Tests.
    /// </summary>
    /// <seealso cref="TestBase" />
    [TestFixture]
    public sealed class HelloWorldProcessorTests : TestBase
    {
        /// <summary>
        /// Gets the get fakes.
        /// </summary>
        /// <value>
        /// The get fakes.
        /// </value>
        [ItemNotNull]
        [NotNull]
        private static IEnumerable<HelloWorldRepoEntity> GetFakes => new List<HelloWorldRepoEntity>
        {
            new HelloWorldRepoEntity { Id = 1, Value = "FakedMock#1" },
            new HelloWorldRepoEntity { Id = 2, Value = "FakedMock#2" },
        };

        /// <summary>
        /// Processes the asynchronous when identifier known expect identifier.
        /// </summary>
        /// <remarks>
        /// <para>This test shows how to test the business logic together with mocking out and verifying the execution of the repository dependency.</para>
        /// </remarks>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        public async Task ProcessAsync_WhenIdKnown_Expect_Id()
        {
            // Arrange
            var mockRepo = new Mock<IRepositoryReadOnly<HelloWorldRepoEntity, int>>();

            mockRepo.Setup(r => r.GetAllAsync(It.IsAny<CancellationToken>())).Returns(() => Task.FromResult(GetFakes));

            var helloWorldProcessor = new HelloWorldProcessor(this.ExceptionProcessor, mockRepo.Object);

            // Act
            var helloWorldProcessorResponse = await helloWorldProcessor.ProcessAsync(new HelloWorldProcessorRequest { IdToGet = 1 }, CancellationToken.None)
                .ConfigureAwait(false);

            // Assert
            mockRepo.Verify(r => r.GetAllAsync(It.IsAny<CancellationToken>()), Times.Once);
            Assert.That(helloWorldProcessorResponse.TotalItemsInList == 2);
            StringAssert.AreEqualIgnoringCase("FakedMock#1", helloWorldProcessorResponse.Message);
        }
    }
}