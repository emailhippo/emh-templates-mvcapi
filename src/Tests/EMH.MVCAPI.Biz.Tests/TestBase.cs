﻿// <copyright file="TestBase.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace EMH.MVCAPI.Biz.Tests
{
    using System;
    using System.IO;
    using System.Reflection;
    using EMH.Lib.Patterns.ExceptionProcessor.Interface;
    using EMH.Lib.Patterns.ExceptionProcessor.Logging;
    using EMH.Lib.Patterns.HippoLog.Core;
    using EMH.Lib.Patterns.HippoLog.SerilogAdapter;
    using EMH.MVCAPI.Biz.Configuration;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Configuration;
    using Serilog;
    using Serilog.Core;
    using Serilog.Events;

    /// <summary>
    /// The test base.
    /// </summary>
    public abstract class TestBase
    {
        /// <summary>
        ///     This executing assembly.
        /// </summary>
        [NotNull]
        protected static readonly Assembly Assembly = Assembly.GetExecutingAssembly();

        /// <summary>
        /// Initializes a new instance of the <see cref="TestBase"/> class.
        /// </summary>
        protected TestBase()
        {
            var levelSwitch = new LoggingLevelSwitch { MinimumLevel = LogEventLevel.Verbose };

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(levelSwitch)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();

            var serilogAdapter = SerilogAdapterFactory.Create();
            HippoLog.Initialize(serilogAdapter);

            var directoryName = Path.GetDirectoryName(Assembly.GetName().CodeBase);
            if (directoryName != null)
            {
                this.ExecutionPath = directoryName.Replace("file:\\", string.Empty);
            }

            var builder = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

#if DEBUG
            builder.AddUserSecrets<TestBase>();
#endif

            var configuration = new AppConfiguration();

            var configurationRoot = builder.Build();

            configurationRoot.Bind("AppConfiguration", configuration);

            this.ExceptionProcessor = LoggingExceptionProcessorFactory.Create();
        }

        /// <summary>
        ///     Gets the execution path.
        /// </summary>
        [NotNull]
        protected string ExecutionPath { get; }

        /// <summary>
        /// Gets the exception processor.
        /// </summary>
        /// <value>
        /// The exception processor.
        /// </value>
        [NotNull]
        protected IExceptionProcessor ExceptionProcessor { get; }

        /// <summary>
        /// The write time elapsed.
        /// </summary>
        /// <param name="timerElapsed">
        /// The timer elapsed.
        /// </param>
        protected void WriteTimeElapsed(long timerElapsed)
        {
            HippoLog.LogInformation("Elapsed timer: {0}ms", timerElapsed);
        }

        /// <summary>
        /// The write time elapsed.
        /// </summary>
        /// <param name="timerElapsed">
        /// The timer elapsed.
        /// </param>
        protected void WriteTimeElapsed(TimeSpan timerElapsed)
        {
            HippoLog.LogInformation("Elapsed timer: {0}", timerElapsed);
        }

        /// <summary>
        /// Gets the source data directory.
        /// </summary>
        /// <param name="testDataFolder">The test data folder.</param>
        /// <returns>The string.</returns>
        [NotNull]
        protected string GetSourceDataDirectory([NotNull] string testDataFolder)
        {
            var parentDir = Directory.GetParent(Directory.GetParent(Directory.GetParent(this.ExecutionPath).FullName).FullName).FullName;

            var sourceDataDirectory = Path.Combine(parentDir, testDataFolder);

            return sourceDataDirectory;
        }
    }
}