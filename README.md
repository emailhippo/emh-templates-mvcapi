# Email Hippo MVC API Project Template

## About
This is the source for the Email Hippo dotnet MVC API.

## Usage

__1) Install The Template Locally__
~~~
dotnet new --install EMH.Templates.MVCAPI
~~~

__2) Create a new project from Local Template__
~~~
dotnet new emh-webapi -n {YourProjectName}
~~~
